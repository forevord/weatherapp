//
//  DataManager.swift
//  weatherInLocation
//
//  Created by Pavel Salkevich on 25.07.17.
//  Copyright © 2017 Pavel Salkevich. All rights reserved.
//

import Foundation

enum DataManagerError: Error {
    
    case Unknown
    case FailedRequest
    case InvalidResponse
    
}

//final class DataManager: NSObject {

class DataManager {

//    typealias WeatherDataCompletion = (JSONDecoder?, DataManagerError?) -> ()
    typealias WeatherDataCompletion = (AnyObject?, DataManagerError?) -> ()
//    typealias WeatherDataCompletionFinal = (WeatherData?, DataManagerError?) -> ()
    
    let baseURL: URL
    
    // MARK: - Initialization
    
    init(baseURL: URL) {
        self.baseURL = baseURL
    }
    
    // MARK: - Requesting Data
    
    func weatherDataForLocation(latitude: Double, longitude: Double, completion: @escaping WeatherDataCompletion) {
        // Create URL
        let URL = baseURL.appendingPathComponent("\(latitude),\(longitude)")
        
        // Create Data Task
        URLSession.shared.dataTask(with: URL) { (data, response, error) in
            self.didFetchWeatherData(data: data, response: response, error: error, completion: completion)
            }.resume()
    }
    
    // MARK: - Helper Methods
    
    private func didFetchWeatherData(data: Data?, response: URLResponse?, error: Error?, completion: WeatherDataCompletion) {
        if let _ = error {
            completion(nil, .FailedRequest)
            
        } else if let data = data, let response = response as? HTTPURLResponse {
            if response.statusCode == 200 {
                processWeatherData(data: data, completion: completion)
            } else {
                completion(nil, .FailedRequest)
            }
            
        } else {
            completion(nil, .Unknown)
        }
    }
    
    private func processWeatherData(data: Data, completion: WeatherDataCompletion) {
        
        let JSDecode = try? JSONDecoder.init(data: data)
        let ObjW = try? WeatherData.init(decoder: JSDecode!)
        
        if ObjW != nil{
            completion(ObjW as AnyObject, nil)
        } else {
            completion(nil, .InvalidResponse)
        }
        
//        let ourTemperature = ObjW?.temperature
////        print("JSDecode: \(JSDecode)")
//        print("Weather obj: \(ObjW)")
//        print("Weather obj: \(ourTemperature!)")

//        if let JSON = try? JSONSerialization.jsonObject(with: data, options: []) as AnyObject {
//            completion(JSON as? JSONDecoder, nil)
////            print("WeatherData:\(JSON)")
//            
//        } else {
//            completion(nil, .InvalidResponse)
//            print("Complition with error")
//        }
    }
    
}
