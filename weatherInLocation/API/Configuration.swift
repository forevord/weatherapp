//
//  Configuration.swift
//  weatherInLocation
//
//  Created by Pavel Salkevich on 25.07.17.
//  Copyright © 2017 Pavel Salkevich. All rights reserved.
//

import Foundation

struct API {
    static let APIKey = "ce922eff24a03f6c05812ea61bdc4b5c"
    static let BaseURL = URL(string: "https://api.forecast.io/forecast/")!
    
    static var AuthenticatedBaseURL: URL {
        return BaseURL.appendingPathComponent(APIKey)
    }
}

struct Defaults {
    
    static let Latitude: Double = 37.8267
    static let Longitude: Double = -122.423
    
}
