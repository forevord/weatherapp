//
//  LocationData.swift
//  weatherInLocation
//
//  Created by Pavel Salkevich on 04.08.17.
//  Copyright © 2017 Pavel Salkevich. All rights reserved.
//

import Foundation

class LocationData {
    
    let city: String
    let country: String
    let street: String
    let name: String
    let zip: String
    let fullAddress: String

    init(addressDict: [AnyHashable : Any]) {
        self.city = (addressDict["City"] as? String)!
        self.country = (addressDict["Country"] as? String)!
        self.street = (addressDict["Thoroughfare"] as? String)!
        self.name = (addressDict["Name"] as? String)!
        self.zip = (addressDict["ZIP"] as? String)!
        let formattedAddress = addressDict["FormattedAddressLines"] as? [String]
        self.fullAddress = (formattedAddress?.joined(separator: ", "))!
    }
}

