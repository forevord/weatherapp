//
//  CheckLocationViewController.swift
//  weatherInLocation
//
//  Created by Pavel Salkevich on 25.07.17.
//  Copyright © 2017 Pavel Salkevich. All rights reserved.
//

import UIKit
import CoreLocation

class CheckLocationViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var activeIndicate: UIActivityIndicatorView!
    @IBOutlet weak var longitudeLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    let locationManager = CLLocationManager()
    var currentLocation:CLLocation?
    @IBAction func newRequest(_ sender: Any) {
        locationManager.startUpdatingLocation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last, location.horizontalAccuracy >= 0 else { return }
        activeIndicate.startAnimating()
        currentLocation = location
        manager.stopUpdatingLocation()
        geoDecode()

    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func geoDecode() {
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: (currentLocation?.coordinate.latitude)!, longitude: (currentLocation?.coordinate.longitude)!)
        
        print("--------------------------------------")
        print("Checking location: \(currentLocation!)")
        print("--------------------------------------")
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error in
            guard let addressDict = placemarks?[0].addressDictionary else {
                return
            }
            // Print each key-value pair in a new row
            print("Geo Reverse: -------------------------")
                addressDict.forEach { print($0) }
            print("--------------------------------------")
            let locationObject = LocationData.init(addressDict: addressDict)
            self.checkWeather(locationObject: locationObject)
        })
    }
    
    func checkWeather(locationObject:LocationData){
        if let locationWeather = currentLocation {
            let initDataObj = DataManager(baseURL: API.AuthenticatedBaseURL)
            initDataObj.weatherDataForLocation(latitude: locationWeather.coordinate.latitude, longitude: locationWeather.coordinate.longitude, completion: {result, error in
                if let weatherObjectResponse = result{
                    let finalWeatherData = weatherObjectResponse as! WeatherData
                    self.saveResultToCoreData(locationResult: locationObject, weatherResult: finalWeatherData)
                }
            })
        }
    }
    
    func saveResultToCoreData(locationResult:LocationData, weatherResult:WeatherData){
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let core = WeatherCore(context: context)
            core.temperature = String(weatherResult.temperature)
            core.latitude = String(weatherResult.lat)
            core.longitude = String(weatherResult.long)
            core.date = weatherResult.time as NSDate
            core.city = locationResult.city
            core.address = locationResult.fullAddress
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            let _ = self.navigationController?.popViewController(animated: true)
        updateLabels(temperature: String(weatherResult.temperature), fullAddress: locationResult.fullAddress)
        }
    
    func updateLabels(temperature:String, fullAddress:String) {
        
        if let bindingLatitude = currentLocation?.coordinate.latitude {
            latitudeLabel.text = String(describing: bindingLatitude)
        }
        if let bindingLongitude = currentLocation?.coordinate.longitude {
            longitudeLabel.text = String(describing: bindingLongitude)
        }
            temperatureLabel.text = temperature + " °C"
        addressLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        addressLabel.numberOfLines = 2
        addressLabel.text = fullAddress
        activeIndicate.stopAnimating()
        activeIndicate.hidesWhenStopped = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
