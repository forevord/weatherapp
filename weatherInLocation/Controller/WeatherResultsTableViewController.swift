//
//  WeatherResultsTableViewController.swift
//  weatherInLocation
//
//  Created by Pavel Salkevich on 25.07.17.
//  Copyright © 2017 Pavel Salkevich. All rights reserved.
//

import UIKit

class WeatherResultsTableViewController: UITableViewController {

    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var weatherResponse: [WeatherCore] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getData()
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherResponse.count
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "mySegue", sender: nil)
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") else {
                return UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "Cell")
            }
            return cell
        }()
        
        let weatherObj = weatherResponse[indexPath.row]
        if let date = weatherObj.date {
            cell.textLabel?.text = String(describing: date)
        }
        if let lat = weatherObj.latitude, let long = weatherObj.longitude, let city = weatherObj.city {
            cell.detailTextLabel?.text = lat + " / " + long + " - " + city
        }

        return cell
    }
    
    func getData() {
        do {
            weatherResponse = try context.fetch(WeatherCore.fetchRequest())
        }
        catch {
            print("Fetching Failed")
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let weatherObj = weatherResponse[indexPath.row]
            context.delete(weatherObj)
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            
            do {
                weatherResponse = try context.fetch(WeatherCore.fetchRequest())
            }
            catch {
                print("Fetching Failed")
            }
        }
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mySegue" ,
            let nextScene = segue.destination as? FullResultController,
            let indexPath = self.tableView.indexPathForSelectedRow {
            let selectedLocation = weatherResponse[indexPath.row]
            if let lat = selectedLocation.latitude, let long = selectedLocation.longitude {
                nextScene.longitudeAndLatitude = lat + " / " + long
            }
            if let date = selectedLocation.date {
                nextScene.timeAndDate = String(describing: date)
            }
            if let address = selectedLocation.address {
                nextScene.fullAddress = address
            }
            if let temperature = selectedLocation.temperature {
                nextScene.weatherTemperature = temperature
            }
        }
    }

}
