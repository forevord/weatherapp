//
//  FullResultController.swift
//  weatherInLocation
//
//  Created by Pavel Salkevich on 25.07.17.
//  Copyright © 2017 Pavel Salkevich. All rights reserved.
//

import UIKit

class FullResultController: UIViewController {
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var latAndLongLabel: UILabel!
    @IBOutlet weak var timeAndDateLabel: UILabel!
    var longitudeAndLatitude = ""
    var fullAddress = ""
    var timeAndDate = ""
    var weatherTemperature = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        latAndLongLabel.text = longitudeAndLatitude
        addressLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        addressLabel.numberOfLines = 2
        addressLabel.text = fullAddress
        timeAndDateLabel.text = timeAndDate
        weatherLabel.text = weatherTemperature + " °C"
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
